from pathlib import Path
from datetime import date

from sentinelsat.sentinel import SentinelAPI, read_geojson, geojson_to_wkt


def main():
    # connect to the API
    api = SentinelAPI(
        user='pechiera',
        password='AgatheP2525',
        api_url='https://scihub.copernicus.eu/dhus',
    )

    json_file = Path(__file__).resolve().parents[0].joinpath("../data/jsonfile.json")

    # search by polygon, time, and Hub query keywords
    footprint = geojson_to_wkt(read_geojson(f"{json_file}"))
    products = api.query(
        footprint=footprint,
        date=(date(2022, 4, 1), date(2022, 5, 1)),  # choice of time interval
        platformname='Sentinel-2',  # choice of satellite
        processinglevel='Level-2A',
        cloudcoverpercentage=(0, 10),  # choice of cloud cover
    )

    api.download_all(products)


if __name__ == "__main__":
    main()

