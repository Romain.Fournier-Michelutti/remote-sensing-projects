import numpy as np
import cv2
import matplotlib.pyplot as plt
import time


def Bicubic(pan, hs):
    M, N, c = pan.shape
    m, n, C = hs.shape
    ratio = int(np.round(M/m))
    print('get sharpening ratio: ', ratio)
    assert int(np.round(M/m)) == int(np.round(N/n))
    I_Bicubic = cv2.resize(hs, (n*ratio, m*ratio), cv2.INTER_CUBIC)
    return np.uint8(I_Bicubic)


def main():
    start = time.time()

    hs = cv2.imread('../data/1.TIF', -1)
    pan = cv2.imread('../data/1.TIF', -1)

    a = Bicubic(pan, hs)
    cv2.imwrite('../data/output/mod.TIF', a)
    # cv2.imwrite('init.TIF', hs)
    # cv2.imwrite('panch.TIF', pan)
    # plt.imshow(pan)
    # plt.show()
    # plt.imshow(hs[:,:,::-1])
    # plt.show()
    plt.imshow(a[:, :, ::-1])
    plt.show()

    end = time.time()
    print(end - start)


if __name__ == "__main__":
    main()
