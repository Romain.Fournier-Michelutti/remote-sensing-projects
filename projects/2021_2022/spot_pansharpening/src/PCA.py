import numpy as np
from utils import upsample_interp23
from sklearn.decomposition import PCA as princomp
import matplotlib.pyplot as plt
import cv2
import time

start = time.time()

def PCA(pan, hs):

    M, N, c = pan.shape
    m, n, C = hs.shape
    
    ratio = np.int(np.round(M/m))
        
    print('get sharpening ratio: ', ratio)
    assert int(np.round(M/m)) == int(np.round(N/n))
    
    image_hr = pan
    
    u_hs = upsample_interp23(hs, ratio)
    
    p = princomp(n_components=C)
    pca_hs = p.fit_transform(np.reshape(u_hs, (M*N, C)))
    
    pca_hs = np.reshape(pca_hs, (M, N, C))
    
    I = pca_hs[:, :, 0]
    
    image_hr = (image_hr - np.mean(image_hr))*np.std(I, ddof=1)/np.std(image_hr, ddof=1)+np.mean(I)
    
    pca_hs[:, :, 0] = image_hr[:, :, 0]
    
    I_PCA = p.inverse_transform(pca_hs)

    #equalization
    I_PCA = I_PCA-np.mean(I_PCA, axis=(0, 1))+np.mean(u_hs)
    
    return np.uint8(I_PCA*255)

hs=cv2.imread('Z:\\1A\\S2\\SPOTTT\\1.TIF')
pan=cv2.imread('Z:\\1A\\S2\\SPOTTT\\3.TIF')

# pixel_start = 1000
# pixel_end = 2000

# pan_crop = pan[pixel_start*4 : pixel_end*4, pixel_start*4 : pixel_end*4]
# ms_crop = init[pixel_start: pixel_end, pixel_start: pixel_end]

a=PCA(np.array(pan, dtype=np.float64), hs)
cv2.imwrite('mod2.TIF', a)
# cv2.imwrite('init.TIF', hs)
# cv2.imwrite('panch.TIF', pan)
# plt.imshow(pan)
# plt.show()
# plt.imshow(hs[:,:,::-1])
# plt.show()
plt.imshow(a[:,:,::-1])
plt.show()

end = time.time()

print(end - start)