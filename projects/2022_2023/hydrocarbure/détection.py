# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


import skimage 
from skimage import io
skimage.__version__
from skimage import exposure
import matplotlib.pyplot as plt
import numpy as np
import rasterio
import math
"""import rasterio"""


img = io.imread("/eau2.PNG")
img1 = io.imread("/blanc.jpg")
height_img=img.shape[0]
width_img=img.shape[1]
surface_img=height_img*width_img




def repartition(img):
    fig, axs = plt.subplots(nrows = 1, ncols = 3, figsize = (13,4))
    canals = ['rouge', 'vert', 'bleu']
    for i, canal in enumerate(canals):
        hist = exposure.histogram( img[ : , : , (i)] )
        axs[i].plot(hist[0])
        axs[i].set_title(canal)


def pixelnoire(img):
    pixel_noire=np.array([0,0,0,255])
    pixel_blanc=np.array([255,255,255,255])
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i][j][0]<70:
                img[i][j]=pixel_noire
            else:
                img[i][j]=pixel_blanc
    
    
    
                
    
def detectionzone(img,w,v):
    nb_pixelnoire=0
    for i in range((w-1)*(img.shape[0])//40,w*(img.shape[0])//40):
        for j in range((v-1)*(img.shape[1])//40,v*(img.shape[1])//40):
            if img[i][j][0]<70:
                nb_pixelnoire+=1
            
    
            
    return nb_pixelnoire
                
                
def restriction(img):
     pixel_noire=np.array([0,0,0,255])
     pixel_blanc=np.array([255,255,255,255])
     for w in range(1,41):
         for v in range(1,41):
             for i in range((w-1)*(img.shape[0])//40,w*(img.shape[0])//40):
                 for j in range((v-1)*(img.shape[1])//40,v*(img.shape[1])//40):
                     a= detectionzone(img,w,v)
                     if a<38:
                         img[i][j]= pixel_blanc
                     else:
                        img[i][j]= pixel_noire
     
                
        
        

def pixelnoirefinale(img):
    restriction(img)
    plt.imshow(img)
    pixel_noire=np.array([0,0,0,255])
    s=0
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i][j][0]<70:
                img[i][j]=pixel_noire
                s+=1
    return s
    
            
    




img = io.imread("/eau2.PNG")
# Ouverture de l'image Sentinel-1
with rasterio.open("/eau2.PNG") as src:
    # Lecture de la première bande de polarisation
    hh = src.read(1)
    vv= src.read(3)
    mean_vv = np.mean(vv)
    sigma_0=np.zeros((hh.shape[0],hh.shape[1]))
for i in range(img.shape[0]):
    for j in range(img.shape[1]):
     sigma_0[i][j] = (hh[i][j] + vv[i][j]) / (2**0.5)




    
   


def Pauli():
    pixel_noire=np.array([0,0,0,255])
    pixel_blanc=np.array([255,255,255,255])
    im=[]
    for i in range(vv.shape[0]):
        for j in range(vv.shape[1]):
            if (hh[i][j] + vv[i][j]) / np.sqrt(2)< 100:
                img[i][j]= pixel_noire
            else:
                img[i][j]=pixel_blanc
            im.append(img[i][j])
    plt.imshow(img)
    return im          




def rangeVV():
    pixel_noire=np.array([0,0,0,255])
    pixel_blanc=np.array([255,255,255,255]) 
    im=[]
    for i in range(vv.shape[0]):
        for j in range(vv.shape[1]):
            if vv[i][j]<mean_vv -70  :
                img[i][j]= pixel_noire
            else:
                img[i][j]=pixel_blanc
            im.append(img[i][j])
    plt.imshow(img)
    return im





    



    
    
     
     
           
        
    
    