# -*- coding: utf-8 -*-

#different librairies that we will use 
import rasterio                    #in order to read images 
import numpy as np                 #in order to treat images 
import matplotlib.pyplot as plt    #in order to display images

#path of different images

b2 = 'Z:/BE_teledetection/06.08.2020/T40KEC_20200806T062449_B02_10m.jp2'
b3 = 'Z:/BE_teledetection/06.08.2020/T40KEC_20200806T062449_B03_10m.jp2'
b4 = 'Z:/BE_teledetection/06.08.2020/T40KEC_20200806T062449_B04_10m.jp2'
b8 = 'Z:/BE_teledetection/06.08.2020/T40KEC_20200806T062449_B08_10m.jp2'


def readSAR(path_b2, path_b3, path_b4):
    b2 = rasterio.open(path_b2)
    b3 = rasterio.open(path_b3)
    b4 = rasterio.open(path_b4)
    red = b4.read(1).astype('float32')    #conversion in float 
    green = b3.read(1).astype('float32')   
    blue = b2.read(1).astype('float32')
    red = (red - np.min(red)) / (np.max(red) - np.min(red))     #normalization in order to have intensity beteween 0 and 1
    green = (green - np.min(green)) / (np.max(green) - np.min(green))
    blue = (blue - np.min(blue)) / (np.max(blue) - np.min(blue))
    return np.dstack((red, green, blue))    #in order to have a RGB image

def readband(path_b):
    b = rasterio.open(path_b)
    band = b.read(1).astype('float32')
    return (band - np.min(band))/(np.max(band) - np.min(band))


def brt(img, coeff):    #in order to enhnance the brightness of the image
    return img*coeff       

def scaling(img,b,h,g,d):   #in order to zoom on a particular place
    return img[b:h,g:d]     

def showim(img):    #in order to display an image
    plt.imshow(img)
    plt.show()
    
def ndwi(path_b3, path_b8):     #in order to have a NDWI image
    b3 = rasterio.open(path_b3)
    b8 = rasterio.open(path_b8)
    green = b3.read(1).astype('float32')
    PIR = b8.read(1).astype('float32')
    green = (green - np.min(green)) / (np.max(green) - np.min(green))
    PIR = (PIR - np.min(PIR)) / (np.max(PIR) - np.min(PIR))
    NDWI = (green - PIR)/(green + PIR)
    return NDWI

def filter_(img, seuil):    #in order to isolate particular pixel in function of their intensity
    h = np.shape(img)[0]
    l = np.shape(img)[1]
    img1 = np.copy(img)
    for i in range (h):
        for j in range (l):
            if img[i][j] < seuil:
                img1[i][j] = 1.
            else:
                img1[i][j] = 0.
    return img1
       

im = readSAR(b2,b3,b4)      #image of Mauritius Island
im_lum = brt(im,4)
im_part = scaling(im_lum,5900,6100,7650,7800)   #zomm on oil spill

NDWI = ndwi(b3, b8)
NDWI_part = scaling(NDWI,5900,6100,7650,7800)

b2_part = scaling(readband(b2),5900,6100,7650,7800)
b3_part = scaling(readband(b3),5900,6100,7650,7800)
b4_part = scaling(readband(b4),5900,6100,7650,7800)
b8_part = scaling(readband(b8),5900,6100,7650,7800)

b2_filtre = filter_(b2_part, 0.036)
b3_filtre = filter_(b3_part, 0.04)
imb2_filtre = filter_(readband(b2), 0.036)

showim(im)
showim(im_part)
#showim(b2_part)
#showim(b3_part)
#showim(b4_part)
#showim(b8_part)

showim(b2_filtre)
showim(b3_filtre)
#showim(imb2_filtre)




